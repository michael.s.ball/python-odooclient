# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from odooclient.common import BasicManager


class ProductManager(BasicManager):

    def __init__(self, *args, **kwargs):
        super(ProductManager, self).__init__(*args, **kwargs)
        self.product_cache = {}

    def find_product(self, name):

        if name not in self.product_cache:
            ps = self.list(
                read=True,
                name=name,
                sale_ok=True,
                active=True,
                company_id=11,
            )
            if len(ps) > 1:
                self.log.warning(
                    f"WARNING: {len(ps)} products found for {name}"
                )

            if len(ps) == 0:
                self.log.error(f"ERROR: no matching product for {name}")
                return None

            self.product_cache[name] = ps[0]

        return self.product_cache[name]

    def get_price(self, pricelist, product, volume):
        price = self.client.price_lists.resource_env.price_get(
            [pricelist], product["id"], volume if volume >= 0 else 0
        )[str(pricelist)]

        return price if volume >= 0 else -price
