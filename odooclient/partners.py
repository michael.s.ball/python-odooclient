# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import odoorpc

from odooclient.common import BasicManager


class PartnerManager(BasicManager):

    default_fields = [
        # Core fields:
        'id',
        'name',
        'email',
        'phone',
        'property_product_pricelist',
        'category_id',
        'user_id',
        # OpenStack fields:
        'company_type',
        'customer_rank',
        'stripe_customer_id',
        'os_projects',
        'os_project_contacts',
        'os_customer_group',
        'os_reseller',
        'os_trial',
        'os_referral',
        'os_referral_codes',
        'street',
        'street2',
        'zip',
        'city',
        'country_id',
    ]

    def add_internal_note(self, partner_id, message_body, **kwargs):
        """Set a note on the given partner"""

        if type(partner_id) != int:
            partner_id = partner_id.id

        body = {
            'body': message_body,
            'res_id': partner_id,
            'partner_ids': [],
            'model': 'res.partner',
            'message_type': 'comment',
        }
        body.update(kwargs)
        self.client.mail_messages.create(**body)
